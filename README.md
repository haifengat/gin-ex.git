# gin-ex

#### 介绍

gin 常用方式封装

#### 软件架构

软件架构说明

```go
package ge

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
)

func TestGet(t *testing.T) {
	go func() {
		time.Sleep(3 * time.Second)
		t.Run("testGet", func(t *testing.T) {
			rsp, err := http.Get(`http://localhost:9000/testGet?pageInfo={"pageNo":1,"pageSize":10}&qry={"TmplID":"01001","DepartID":"华北5"}&append="active='1'"`)
			bs, _ := io.ReadAll(rsp.Body)
			fmt.Println(string(bs), err)
		})
	}()
	e := gin.Default()
	e.GET("/testGet", CreateHandler(func(ctx *gin.Context) (data any, data2 any, errRtn error) {
		p, where, append := GetQryParams(ctx)
		data2 = *p
		data = where
		fmt.Println("append", append)
		return
	}))
	e.Run(":9000")
}

func TestPut(t *testing.T) {
	go func() {
		time.Sleep(3 * time.Second)
		t.Run("testPut", func(t *testing.T) {
			rsp, err := http.NewRequest("PUT", `http://localhost:9000/testPut`, strings.NewReader(`{"where": {"TmplID":"01001","DepartID":"华北5"},"set":{"active": "1"}}`))
			bs, _ := io.ReadAll(rsp.Body)
			fmt.Println(string(bs), err)
		})
	}()
	e := gin.Default()
	e.PUT("/testPut", CreateHandler(func(ctx *gin.Context) (data any, data2 any, errRtn error) {
		var append string
		data, data2, append = GetPutParams(ctx)
		fmt.Println("append", append)
		return
	}))
	e.Run(":9000")
}
func TestPost(t *testing.T) {
	go func() {
		time.Sleep(3 * time.Second)
		t.Run("testPost", func(t *testing.T) {
			rsp, err := http.Post(`http://localhost:9000/testPost`, "application/json", strings.NewReader(`{"TmplID":"01001","DepartID":"华北5"}`))
			bs, _ := io.ReadAll(rsp.Body)
			fmt.Println(string(bs), err)
		})
		t.Run("testPosts", func(t *testing.T) {
			rsp, err := http.Post(`http://localhost:9000/testPost`, "application/json", strings.NewReader(`[{"TmplID":"01001","DepartID":"华北5"},{"TmplID":"01002","DepartID":"华北3"}]`))
			bs, _ := io.ReadAll(rsp.Body)
			fmt.Println(string(bs), err)
		})
	}()
	e := gin.Default()
	e.POST("/testPost", CreateHandler(func(ctx *gin.Context) (data any, data2 any, errRtn error) {
		data = GetPostParams(ctx)
		return
	}))
	e.Run(":9000")
}
```
