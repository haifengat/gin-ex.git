<!--
 * @Author: haifengat hubert28@qq.com
 * @Date: 2023-05-25 10:46:54
 * @LastEditors: haifengat hubert28@qq.com
 * @LastEditTime: 2023-06-01 16:14:45
 * @FilePath: /gin-ex/CHANGELOG.md
 * @Description: 更新记录
-->

# 更新日志

- 新增: PutParams 支持单个 struct(where 为空, set 为 struct)
- 修复: GetQryParams pageInfo 为""时的 bug
- 修复: GetPutParams 返回为空的 bug

## 0.0.4

- 更新: ResponseLog 只显示文本内容
- 更新: 错误响应时也返回 Data Data2

## v0.0.3

- 更新: GetPutParams 不返回 appendSql
- 优化: sqlAppend 为 []string 时, 用 ' ' 连接
- 新增: GetPutParams 支持 where 数组,多条件查询实现批量更新

## v0.0.1

- 新增: gin_test.go 测试用例
- 新增: 中间件 cors cost showlog
- 新增: CreateHandler 路由, GetQryParams 从 ctx 中取 get 参数, GetPutParams 从 ctx 中取 put 参数, GetPostParams 从 ctx 中取 post 参数
